import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { environment } from '@env';
import { MenuController, AlertController, ActionSheetController, LoadingController } from '@ionic/angular';
@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
	// @ViewChild("navbar1") nav_bar1;
  	// @ViewChild("navbar2") nav_bar2;
  	// @ViewChild("topsearchbar") topsearchbar;
	@Input() showsearch: boolean = false;
  	@Input() showtitle: boolean = false;
  	@Input() titleText: string = '';
  	@Input() hide_menu_btn: boolean = false;
  	@Input() hide_back_btn: boolean = false;
  	@Input() color: string = "primary";
	constructor() { }

	ngOnInit() {}

}
