import { Component } from '@angular/core';
import { Platform, MenuController, ToastController} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


import { NetworkService } from '@services/network.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private networkSvc: NetworkService
  ) {
    this.initializeApp();
  }

  async initializeApp() {
    // Let ready the platform to move on
    let platform = await this.platform.ready();

    this.statusBar.styleDefault();
    this.splashScreen.hide();

    // Listen to the network interface
    this.networkSvc.listenNetwork();
    if (!this.platform.is("cordova")) {
      this.networkSvc.online = true;
    }
    // Subscribe for platform pause and resume events
    this.platform.pause.subscribe(e => {
      console.log("[INFO] Application paused....");
    });
    this.platform.resume.subscribe(async e => {
      console.log("[INFO] Application resumed....");
    });


  }
}
