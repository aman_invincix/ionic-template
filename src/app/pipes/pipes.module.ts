import { NgModule } from '@angular/core';
import { SafePipe } from '@pipes/safepipe.pipe';
import { JsonPipe } from '@pipes/jsonpipe.pipe';

@NgModule({
	declarations: [
		SafePipe,
		JsonPipe
	],
	exports: [
		SafePipe,
		JsonPipe
	]
})
export class PipesModule {}
